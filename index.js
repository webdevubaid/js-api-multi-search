// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.
// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
function initAutocomplete() {


    const map = new google.maps.Map(document.querySelector("#map"), {
      center: { lat: -33.8688, lng: 151.2195 },
      zoom: 13,
      mapTypeId: "roadmap",
    });
    const map_sec = new google.maps.Map(document.querySelector(".map2"), {
      center: { lat: -32.8688 ,lng  : 151.2195 },
      zoom: 13,
      mapTypeId: "roadmap",
    });
    // Create the search box and link it to the UI element.
    const input = document.getElementById("pac-input");
    const input2 = document.getElementById("pac-input-sec");
    const searchBox = new google.maps.places.SearchBox(input);
    const searchBox2 = new google.maps.places.SearchBox(input2);
  
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    map_sec.controls[google.maps.ControlPosition.TOP_LEFT].push(input2);
    // Bias the SearchBox results towards current map's viewport.
    map.addListener("bounds_changed", () => {
      searchBox.setBounds(map.getBounds());
    });
    map_sec.addListener("bounds_changed", () => {
      searchBox2.setBounds(map_sec.getBounds());
    });
  
    let markers = [];
    let markers2 = [];
    const lat1 = {};
    const lng1 = {};
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener("places_changed", () => {
      const places = searchBox.getPlaces();
  
      if (places.length == 0) {
        return;
      }
  
      // Clear out the old markers.
      markers.forEach((marker) => {
        marker.setMap(null);
      });
      markers = [];
  
      // For each place, get the icon, name and location.
      const bounds = new google.maps.LatLngBounds();
      
      places.forEach((place) => {
        if (!place.geometry || !place.geometry.location) {
          console.log("Returned place contains no geometry");
          return;
        }
  
        const icon = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25),
        };
  
        // Create a marker for each place.
        markers.push(
          new google.maps.Marker({
            map,
            icon,
            title: place.name,
            position: place.geometry.location,
          })
        );
        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
        lat1.latitude = place.geometry.location.lat();
        lng1.longitude = place.geometry.location.lng();
      });
      map.fitBounds(bounds);

      console.log(lat1.latitude);
      console.log(lng1.longitude);

    });

    // sec input
    const lat2 = {};
    const lng2 = {};
    searchBox2.addListener("places_changed", () => {
      const places2 = searchBox2.getPlaces();
  
      if (places2.length == 0) {
        return;
      }
  
      // Clear out the old markers.
      markers2.forEach((marker) => {
        marker.setMap(null);
      });
      
      markers2 = [];
  
      // For each place, get the icon, name and location.
      const bounds2 = new google.maps.LatLngBounds();
      
      places2.forEach((place2) => {
        if (!place2.geometry || !place2.geometry.location) {
          console.log("Returned place contains no geometry");
          return;
        }
        const icon2 = {
          url: place2.icon2,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25),
        };
  
        // Create a marker for each place.
        markers2.push(
          new google.maps.Marker({
            map_sec,
            icon2,
            title: place2.name,
            position: place2.geometry.location,
          })
        );
        if (place2.geometry.viewport) {
          // Only geocodes have viewport.
          bounds2.union(place2.geometry.viewport);
        } else {
          bounds2.extend(place2.geometry.location);
          
        }
        lat2.latitude = place2.geometry.location.lat();
        lng2.longitude = place2.geometry.location.lng();
        
      });
      map_sec.fitBounds(bounds2);
      console.log(lat2.latitude);
      console.log(lng2.longitude);
      
      // console.log(longitude);
      function getDistanceBetweenPoints(){

        // The radius of the planet earth in meters
        alat = lat1.latitude;
        alng = lng1.longitude;
        blat = lat2.latitude;
        blng = lng2.longitude;
        let R = 6378137;
        let dLat = blat - alat;
        let dLong = blng - alng;
        let a = Math.sin(dLat / 2)
                *
                Math.sin(dLat / 2) 
                +
                Math.cos(alat) 
                * 
                Math.cos(alat) 
                *
                Math.sin(dLong / 2) 
                * 
                Math.sin(dLong / 2);
    
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        let distance = R * c;
    
        console.log(distance/1000);
    }getDistanceBetweenPoints(); 
   
    });
}
  